def clean(text)
  text.gsub(/[^\w\s]/," ")
end
def coupe(text)
  text=text.split("")
end
def compteOcurrence(lettre)
  nb={}
  lettre.each{|let| nb[let]=lettre.count(let)}
  return nb
end

def total_Occurence(occurence)
  tot=0
  occurence.each do |key, value| 
        tot=tot+value
  end
  return tot
end


def affichage(occurence)
  occurence.each do |key, value| 
       print "'"+key+"'" + ":" + value.to_s + " , "
  end
end



if $0 == __FILE__
  if ARGV.empty?
        exit 0
  end

  file_path = ARGV[0]

  file_content = File.read(file_path)
  cleaned_text = clean(file_content)
  letters = coupe(cleaned_text)
  occurence = compteOcurrence(letters)
  total=total_Occurence(occurence)
  print "{'"+__FILE__ +"':"+total.to_s+" , "
  affichage(occurence)
  print "}\n"
end